<?php

use Illuminate\Support\Str;
use Georgeff\Token\Factory;
use Georgeff\Token\TokenInterface;

class FactoryTest extends PHPUnit_Framework_TestCase
{
    public function testInit()
    {
        $this->assertInstanceOf(TokenInterface::class, Factory::init(Str::random(32)));
    }
}