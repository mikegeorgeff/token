<?php

use Georgeff\Token\Token;
use Illuminate\Support\Str;
use Georgeff\Token\TokenInterface;
use Illuminate\Encryption\Encrypter;

class TokenTest extends PHPUnit_Framework_TestCase
{
    public function testImplementsInterface()
    {
        $token = new Token(Str::random(32));

        $this->assertInstanceOf(TokenInterface::class, $token);
    }

    public function testGetEncrypter()
    {
        $token = new Token(Str::random(32));

        $this->assertInstanceOf(Encrypter::class, $token->getEncypter());
    }

    public function testEncode()
    {
        $token = new Token(Str::random(32));

        $value = $token->encode(['foo' => 'bar']);

        $this->assertInternalType('string', $value);
    }

    public function testDecode()
    {
        $token = new Token(Str::random(32));

        $value = $token->encode(['foo' => 'bar']);

        $data  = $token->decode($value);

        $this->assertEquals(['foo' => 'bar'], $data);
    }
}