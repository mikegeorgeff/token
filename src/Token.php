<?php

namespace Georgeff\Token;

use Illuminate\Encryption\Encrypter;

class Token implements TokenInterface
{
    /**
     * The encryption key
     *
     * @var string
     */
    protected $key;

    /**
     * The encryption cipher
     *
     * @var string
     */
    protected $cipher;

    /**
     * The encrypter instance
     *
     * @var \Illuminate\Encryption\Encrypter
     */
    protected $encrypter;

    /**
     * @param string $key
     * @param string $cipher
     */
    public function __construct($key, $cipher = 'AES-256-CBC')
    {
        $this->key    = $key;
        $this->cipher = $cipher;
    }

    /**
     * {@inheritdoc}
     */
    public function encode(array $data)
    {
        $string = json_encode($data);

        return $this->getEncypter()->encrypt($string);
    }

    /**
     * {@inheritdoc}
     */
    public function decode($token)
    {
        $json = $this->getEncypter()->decrypt($token);

        return json_decode($json, true);
    }

    /**
     * Get the encryter instance
     *
     * @return \Illuminate\Encryption\Encrypter
     */
    public function getEncypter()
    {
        if (is_null($this->encrypter)) {
            $this->encrypter = new Encrypter($this->key, $this->cipher);
        }

        return $this->encrypter;
    }
}