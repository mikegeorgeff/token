<?php

namespace Georgeff\Token;

class Factory
{
    /**
     * Initialize the token class
     * 
     * @param string $key  The encryption key
     * @return \Georgeff\Token\TokenInterface
     */
    public static function init($key)
    {
        return new Token($key);
    }
}