<?php

namespace Georgeff\Token;

interface TokenInterface
{
    /**
     * Generate a token from an array of data
     *
     * @param array $data
     * @return string
     */
    public function encode(array $data);

    /**
     * Return the contents of a token
     *
     * @param string $token
     * @return array
     */
    public function decode($token);
}